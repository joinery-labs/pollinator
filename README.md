# pollinator

using cloudflare workers, can we "plug" third-party open api services? because we will need to repeat this step for different projects.



## Credits

Rust WASM [template](https://github.com/cloudflare/rustwasm-worker-template/) ([LICENSE](https://github.com/cloudflare/rustwasm-worker-template/blob/master/LICENSE_APACHE))

wrangler-action usage by [Klee Thomas](https://dev.to/kleeut/cloudflare-workers-continuous-deployment-with-github-actions-3jo6)

Rust GitHub action by [Michael Yuan](https://www.freecodecamp.org/news/learn-rust-with-github-actions/)

Rustfmt [action](https://dev.to/bampeers/rust-ci-with-github-actions-1ne9) ([LICENSE](https://github.com/BamPeers/rust-ci-github-actions-workflow/blob/main/LICENSE.md))

Rust style [CI](https://github.com/rust-lang/rustfmt#checking-style-on-a-ci-server) 

